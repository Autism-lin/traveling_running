package com.scujcc.travelingrunning.ui.sale;

import com.scujcc.travelingrunning.ui.sale.InterFace.RankApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkFactory {
    public static final String SERVER_URL = "http://192.168.101.2:8080";
    private static Retrofit retrofit;
    private static RankApi rankApi;

    static {
        retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RankApi getRankApi(){
        rankApi = retrofit.create(RankApi.class);
        return rankApi;
    }
}
