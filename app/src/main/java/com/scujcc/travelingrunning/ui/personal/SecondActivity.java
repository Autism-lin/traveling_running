package com.scujcc.travelingrunning.ui.personal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.scujcc.travelingrunning.R;

import java.util.Map;

public class SecondActivity extends AppCompatActivity {
    private PersonalViewModel personalViewModel;
    private EditText et_username;
    private EditText et_password;
    private EditText et_password2;
    private EditText et_mail;
    private Button btn_login;
    private Button btn_register;
    private CheckBox checkbox;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_personaltwo);
        Map<String,String>userInfo =saveinfo.getSaveInformation(this);
        if (userInfo!= null) {
            et_username.setText(userInfo.get("username"));
            et_password.setText(userInfo.get("password"));
        }
        et_username=(EditText)findViewById(R.id.et_username);//找到账户名TextView
        et_password=(EditText)findViewById(R.id.et_password);//找到密码TextView
        checkbox = (CheckBox) findViewById(R.id.checkBox);
        btn_login =(Button) findViewById(R.id.button_login);
        btn_register =(Button) findViewById(R.id.button_register);
        btn_login.setOnClickListener(new MyButton());
        btn_register.setOnClickListener(new MyButton());
    }
    public  class MyButton implements View.OnClickListener{
        @Override
        public void onClick(View view){
            String username = et_username.getText().toString();//获取账户TextView输入内容转换为String 类型
            String password = et_password.getText().toString();//获取密码TextView输入内容转换为String 类型
            switch (view.getId()) {
                //当点击登录按钮时
                case R.id.button_login:
                    if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)){
                        Toast.makeText(SecondActivity.this,"密码或账号不能为空",Toast.LENGTH_SHORT).show();
                    } else {
                        if(checkbox.isChecked()){
                            //保存密码的操作
                        }
                        Toast.makeText(SecondActivity.this,"登录成功",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SecondActivity.this, LoginActivity.class);

                        //向下一个页面传参
                        intent.putExtra("username",username);
                        intent.putExtra("password",password);

                        startActivity(intent);
                    }
                    break;
                //当点击注册按钮事件时
                case R.id.button_register:
                    Intent intent = new Intent(SecondActivity.this,RegisterActivity.class);
                    startActivity(intent);
                    break;

    }
    }}}
