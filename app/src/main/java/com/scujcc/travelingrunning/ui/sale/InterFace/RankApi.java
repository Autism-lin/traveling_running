package com.scujcc.travelingrunning.ui.sale.InterFace;

import com.scujcc.travelingrunning.ui.sale.Info.UserInfo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RankApi {
    @GET("/rank")
    public Call<UserInfo> getRank();
}
