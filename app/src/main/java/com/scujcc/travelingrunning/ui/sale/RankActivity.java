package com.scujcc.travelingrunning.ui.sale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.scujcc.travelingrunning.R;
import com.scujcc.travelingrunning.ui.sale.Info.UserInfo;
import com.scujcc.travelingrunning.ui.sale.InterFace.RankApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RankActivity extends AppCompatActivity {
    public static final String TAG = "MAIN";

    private TextView top1name;
    private TextView top2name;
    private TextView top3name;
    private TextView top1steps;
    private TextView top2steps;
    private TextView top3steps;
    private ImageView top1image;
    private ImageView top2image;
    private ImageView top3image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rank);

        top1name = findViewById(R.id.name_top1);
        top2name = findViewById(R.id.name_top2);
        top3name = findViewById(R.id.name_top3);

        top1steps = findViewById(R.id.steps_top1);
        top2steps = findViewById(R.id.steps_top2);
        top3steps = findViewById(R.id.steps_top3);

        top1image = findViewById(R.id.image_top1);
        top2image = findViewById(R.id.image_top2);
        top3image = findViewById(R.id.image_top3);



        rankInfo();
    }

    private void rankInfo(){
        RankApi rankApi = NetworkFactory.getRankApi();
        Call<UserInfo> u = rankApi.getRank();
        u.enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                UserInfo userInfo = response.body();
                Log.d(TAG,"用户名1" + userInfo.getTop1name());
                Log.d(TAG,"用户名2" + userInfo.getTop2name());
                Log.d(TAG,"用户名3" + userInfo.getTop3name());
                Log.d(TAG,"步数1" + userInfo.getTop1steps());
                Log.d(TAG,"步数2" + userInfo.getTop2steps());
                Log.d(TAG,"步数3" + userInfo.getTop3steps());

                Message message = new Message();
                message.what = 1;
                message.obj = userInfo;
                handler.sendMessage(message);
            }
            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
//                Toast.makeText(RankActivity.this,"访问网络失败，请重试。",Toast.LENGTH_LONG).show();
            }
        });
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            if (msg.what == 0){
                Toast.makeText(RankActivity.this,"访问错误，请重试",Toast.LENGTH_LONG).show();
            }else if(msg.what == 1){
                UserInfo userInfo = (UserInfo) msg.obj;

                top1name.setText((userInfo.getTop1name()));
                top2name.setText((userInfo.getTop1name()));
                top3name.setText((userInfo.getTop1name()));
                top1steps.setText((userInfo.getTop1steps()));
                top2steps.setText((userInfo.getTop2steps()));
                top3steps.setText((userInfo.getTop3steps()));
            }
        }
    };
}