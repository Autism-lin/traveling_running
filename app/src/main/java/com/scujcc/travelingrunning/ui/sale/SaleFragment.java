package com.scujcc.travelingrunning.ui.sale;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scujcc.travelingrunning.R;
import com.scujcc.travelingrunning.databinding.FragmentSaleBinding;

import org.jetbrains.annotations.NotNull;

//商城
public class SaleFragment extends Fragment {

    private SaleViewModel saleViewModel;
    private FragmentSaleBinding binding;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        saleViewModel =
                new ViewModelProvider(this).get(SaleViewModel.class);

    binding = FragmentSaleBinding.inflate(inflater, container, false);
    root = binding.getRoot();

        final TextView textView = binding.textDashboard;
        saleViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

//        实现Fragment页面跳转至Activity
//        Button button = root.findViewById(R.id.sousuofuhao);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SearchActivity.class));
//            }
//        });

        //为三个Button设置点击事件，跳转到各自Activity
        Button button = root.findViewById(R.id.button_weather);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(),WeatherActivity.class));
                Intent intent = new Intent();
                intent.setData(Uri.parse("http://www.weather.com.cn/"));
                startActivity(intent);
            }
        });
        Button button1 = root.findViewById(R.id.button_map);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),MapActivity.class));
            }
        });
        Button button2 = root.findViewById(R.id.button_rank);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),RankActivity.class));
            }
        });
        Button button3 = root.findViewById(R.id.button_help);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setData(Uri.parse("tel:110"));
                startActivity(intent);
            }
        });

        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}