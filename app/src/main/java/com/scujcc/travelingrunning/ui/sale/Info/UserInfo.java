package com.scujcc.travelingrunning.ui.sale.Info;

public class UserInfo {
    private String top1name;
    private String top2name;
    private String top3name;
    private String top1image;
    private String top2image;
    private String top3image;
    private String top1steps;
    private String top2steps;
    private String top3steps;

    public String getTop1name() {
        return top1name;
    }

    public void setTop1name(String top1name) {
        this.top1name = top1name;
    }

    public String getTop2name() {
        return top2name;
    }

    public void setTop2name(String top2name) {
        this.top2name = top2name;
    }

    public String getTop3name() {
        return top3name;
    }

    public void setTop3name(String top3name) {
        this.top3name = top3name;
    }

    public String getTop1image() {
        return top1image;
    }

    public void setTop1image(String top1image) {
        this.top1image = top1image;
    }

    public String getTop2image() {
        return top2image;
    }

    public void setTop2image(String top2image) {
        this.top2image = top2image;
    }

    public String getTop3image() {
        return top3image;
    }

    public void setTop3image(String top3image) {
        this.top3image = top3image;
    }

    public String getTop1steps() {
        return top1steps;
    }

    public void setTop1steps(String top1steps) {
        this.top1steps = top1steps;
    }

    public String getTop2steps() {
        return top2steps;
    }

    public void setTop2steps(String top2steps) {
        this.top2steps = top2steps;
    }

    public String getTop3steps() {
        return top3steps;
    }

    public void setTop3steps(String top3steps) {
        this.top3steps = top3steps;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "top1name='" + top1name + '\'' +
                ", top2name='" + top2name + '\'' +
                ", top3name='" + top3name + '\'' +
                ", top1image='" + top1image + '\'' +
                ", top2image='" + top2image + '\'' +
                ", top3image='" + top3image + '\'' +
                ", top1steps='" + top1steps + '\'' +
                ", top2steps='" + top2steps + '\'' +
                ", top3steps='" + top3steps + '\'' +
                '}';
    }
}
