package com.scujcc.travelingrunning.ui.personal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.scujcc.travelingrunning.R;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        //Intent 接收上一个一个intent传过来的参数
        Intent intent = getIntent();
        String username = intent.getStringExtra("username");//这里的绿色username对应上一个页面的，name：就是hashmap里面的key
        String password = intent.getStringExtra("password");



        TextView name = findViewById(R.id.intent_username);
        TextView pass = findViewById(R.id.intent_password);
        name.setText("登录输入账号为："+username);
        pass.setText("登录输入密码为："+password);


    }
}