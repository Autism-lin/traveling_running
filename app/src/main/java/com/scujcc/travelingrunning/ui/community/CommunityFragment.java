package com.scujcc.travelingrunning.ui.community;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.scujcc.travelingrunning.R;
import com.scujcc.travelingrunning.databinding.FragmentCommunityBinding;

//社区
public class CommunityFragment extends Fragment{

    private CommunityViewModel communityViewModel;
    private FragmentCommunityBinding binding;
    private View root;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        communityViewModel =
                new ViewModelProvider(this).get(CommunityViewModel.class);

    binding = FragmentCommunityBinding.inflate(inflater, container, false);
    root = binding.getRoot();

        Button button1=root.findViewById(R.id.chakan);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),PermActivity.class));
            }
        });

        Button button8 = root.findViewById(R.id.tianjia);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),AddActivity.class));
            }
        });

        Button button7 = root.findViewById(R.id.zhuyet);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),PermActivity.class));
            }
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}