package com.scujcc.travelingrunning.ui.personal;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.scujcc.travelingrunning.R;
import com.scujcc.travelingrunning.databinding.FragmentPersonalBinding;
//个人中心
public class PersonalFragment extends Fragment {

    private PersonalViewModel personalViewModel;
    private FragmentPersonalBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        personalViewModel =
                new ViewModelProvider(this).get(PersonalViewModel.class);

    binding = FragmentPersonalBinding.inflate(inflater, container, false);
    View root = binding.getRoot();

        final TextView textView = binding.btn1;
        personalViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        Button button = root.findViewById(R.id.btn1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(),SecondActivity.class));

            }
        });
       final TextView textView1 = binding.btn2;
       personalViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
           @Override
           public void onChanged(String y) {
               textView1.setText(y); }
       });
       Button button1=root.findViewById(R.id.btn2);
       button1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               startActivity(new Intent(getActivity(),ThreeActivity.class));
           }
       });
        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}